<?php
require 'koneksi.php';

	if( isset($_POST["login"])) {
		$username = $_POST["username"];
		$password = $_POST["password"];

		$result = mysqli_query($conn, "SELECT * FROM tb_admin WHERE username = '$username'");

		//cek username
		if(mysqli_num_rows($result) === 1){
			//cek password
			$row = mysqli_fetch_assoc($result);
			if(password_verify($password, $row["password"]) ) {
				header("Location: home.php");
				exit;
			}
			else{
					header("Location: index.php");
				
			}
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta charset="utf-8">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
		<script src="jquery.min.js"></script>
		<script>
			$(document).ready(function(){
				 $("#getUID").load("UIDContainer.php");
				setInterval(function() {
					$("#getUID").load("UIDContainer.php");
				}, 500);
			});
		</script>
		
		<style>
		html {
			font-family: Arial;
			display: inline-block;
			margin: 0px auto;
		}
		
		textarea {
			resize: none;
		}

		ul.topnav {
			list-style-type: none;
			margin: auto;
			padding: 0;
			overflow: hidden;
			background-color: #4CAF50;
			width: 70%;
		}

		ul.topnav li {float: left;}

		ul.topnav li a {
			display: block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
		}

		ul.topnav li a:hover:not(.active) {background-color: #3e8e41;}

		ul.topnav li a.active {background-color: #333;}

		ul.topnav li.right {float: right;}

		@media screen and (max-width: 600px) {
			ul.topnav li.right, 
			ul.topnav li {float: none;}
		}
		</style>
		
		<title>Institut Teknologi Del</title>
	</head>
	
	<body>

		<h2 align="center">Sistem Informasi Loker Institut Teknologi Del</h2>

		<div class="container">
			<br>
			<div class="center" style="margin: 0 auto; width:495px; border-style: solid; border-color: #f2f2f2;">
				<div class="row">
					<h3 align="center">LOGIN FORM</h3>
				</div>
				<br>
				<form class="form-horizontal" action="login.php" method="post" >
					
					<div class="control-group">
						<label class="control-label">Username</label>
						<div class="controls">
							<input id="div_refresh" name="username" type="text"  placeholder="" required>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Password</label>
						<div class="controls">
							<input name="password" type="password" placeholder="" required>
						</div>
					</div>
					
					
					<div class="form-actions">
						<button type="submit" class="btn btn-info">Login</button>
                    </div>
				</form>
				
			</div>               
		</div> <!-- /container -->	
	</body>
</html>