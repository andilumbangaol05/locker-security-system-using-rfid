<?php
    require 'database.php';
    $id = 0;

    if ( !empty($_GET['id'])) {
     
        $id = $_REQUEST['id'];
    }
     
    if ( !empty($_POST)) {
        // keep track post values
        $id = $_POST['id'];
         
        // delete data
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "DELETE FROM id_tag  WHERE id= ?";

        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        Database::disconnect();
        header("Location: monitoring.php");
         
    }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
	<title>Institut Teknologi Del</title>
</head>
 
<body>
	<h2 align="center">Sistem Informasi Institut Teknologi Del</h2>

    <div class="container">
     
		<div class="span10 offset1">
			<div class="row">
				<h3 align="center">Hapus ID</h3>
			</div>

			<form class="form-horizontal" action="user data delete id.php" method="post">
				<input type="hidden" name="id" value="<?php echo $id;?>"/>
				<p class="alert alert-error">Apakah anda yakin ingin menghapus id?</p>
				<div class="form-actions">
					<button type="submit" class="btn btn-danger" >Ya</button>
					<a class="btn" href="monitoring.php">Tidak</a>
				</div>
			</form>
		</div>
                 
    </div> <!-- /container -->
  </body>
</html>