<?php
    require 'database.php';
    include 'logger.php';
    $id = 0;
     
    if ( !empty($_GET['name'])) {
        $id = $_REQUEST['name'];

   

    }
     
     if ($_SERVER['REQUEST_METHOD'] === "POST") {
         # code...
     
    if ( !empty($_POST)) {
        // keep track post values
        $id = $_POST['name'];  
       

        // delete data
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "DELETE FROM tb_user  WHERE name = ?";

        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        Database::disconnect();
        $log = "Data sudah terhapus milik $id";
        
        logger($log);
        header("Location: user data.php");
         
    }
    else{
        var_dump($sql);
        die();
    }
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
	<title>Institut Teknologi Del</title>
</head>
 
<body>
	<h2 align="center">Sistem Informasi Institut Teknologi Del</h2>

    <div class="container">
     
		<div class="span10 offset1">
			<div class="row">
				<h3 align="center">Hapus Pengguna</h3>
			</div>

			<form class="form-horizontal" action="user data delete page.php" method="post">
				<input type="hidden" name="name" value="<?php echo $id;?>"/>
				<p class="alert alert-error">Apakah anda yakin ingin menghapus user?</p>
				<div class="form-actions">
					<button type="submit" class="btn btn-danger">Ya</button>
					<a class="btn" href="user data.php">Tidak</a>
				</div>
			</form>
		</div>
                 
    </div> <!-- /container -->
  </body>
</html>