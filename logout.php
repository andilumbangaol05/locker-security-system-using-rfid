<?php
session_start();
unset($_SESSION['status_login']);
unset($_SESSION['username']);
unset($_SESSION['password']);
session_destroy();
header('location:index.php'); 
?>