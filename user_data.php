
<?php
	$Write="<?php $" . "UIDresult=''; " . "echo $" . "UIDresult;" . " ?>";
	file_put_contents('UIDContainer.php',$Write);
?>

<!DOCTYPE html>
<html lang="en">
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta charset="utf-8">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
		<style>
		html {
			font-family: Arial;
			display: inline-block;
			margin: 0px auto;
			text-align: center;
		}

		ul.topnav {
			list-style-type: none;
			margin: auto;
			padding: 0;
			overflow: hidden;
			background-color: #008cba;
			width: 70%;
		}

		ul.topnav li {float: left;}

		ul.topnav li a {
			display: block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
		}

		ul.topnav li a:hover:not(.active) {background-color: #bbbbbb;}

		ul.topnav li a.active {background-color: #333;}

		ul.topnav li.right {float: right;}

		@media screen and (max-width: 600px) {
			ul.topnav li.right, 
			ul.topnav li {float: none;}
		}
		
		.table {
			margin: auto;
			width: 90%; 
		}
		
		thead {
			color: #FFFFFF;
		}
		</style>
		<title>Institut Teknologi Del</title>
	</head>
	
	<body>
		<h2>Sistem Informasi Loker Institut Teknologi Del</h2>
		<ul class="topnav">
			<li><a href="home.php">Home</a></li>
			<li><a class="active" href="user data.php">Daftar Loker</a></li>
			<li><a href="registration.php">Registrasi</a></li>
			<li><a href="read tag.php">Scan RFID Tag</a></li>
			<li><a href="http://localhost/phpmyadmin/index.php?route=/database/structure&server=1&db=nodemcu_rfid">Database</a></li>
			<li><a href="monitoring.php">Daftar ID</a></li>
			<li>
			<li><a href="logout.php">Logout</a></li>
		</ul>
		<br>
		<div class="container">
            <div class="row">
                <h3>Tabel Data Pengguna Loker 1</h3>
            </div>
            <div class="row">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr bgcolor="#10a0c5" color="#FFFFFF">
                      <th>Nama</th>
                      <th>ID</th>
					  <th>Jenis Kelamin</th>
					  <th>Prodi</th>
                      <th>NIM</th>

					  <th>Loker</th>
					  <th>Ubah</th>
                    </tr>
                  </thead>
                  <tbody>
                  	
                  <?php
                   include 'database.php';

                   $pdo = Database::connect();

                   $sql = "SELECT * FROM tb_user where loker  = 'Loker 1'";



                   foreach ($pdo->query($sql) as $row) {

                   if($row['name'] == "" || $row['gender'] == "" || $row['prodi'] == "" || $row['nim'] == "" || $row['loker'] == ""){
                   	
                   }
                   else{
                   
                            echo '<tr>';
                            echo '<td>'. $row['name'] . '</td>';         
                            echo '<td>'. $row['id'] . '</td>';
                            echo '<td>'. $row['gender'] . '</td>';
							echo '<td>'. $row['prodi'] . '</td>';
							echo '<td>'. $row['nim'] . '</td>';
							echo '</td>';
							echo '<td>'. $row['loker'] . '</td>';
							echo '<td><a class="btn btn-success" href="user data edit page.php?id='.$row['id'].'">Edit</a>';
							echo ' ';
							echo '<a class="btn btn-danger" href="user data delete page.php?name='.$row['name'].'">Hapus</a>';
							echo '</tr>';

                   
               }
           }
                   Database::disconnect();
                  ?>
                  </tbody>
				</table>
			</div>
		</div> <!-- /container -->
	</body>
</html>

